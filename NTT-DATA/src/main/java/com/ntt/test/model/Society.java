package com.ntt.test.model;

import java.util.Map;
import java.util.HashMap;

public class Society {
	
	private Map<Long, Client> groupClients = new HashMap<Long, Client>();
	
	public Society() {
		Client clientOne = new Client("Adolfina", "Samuel", "Reyes", "Perez", "9612368471", "7ma norte poniente #1850, Av. San Francisco", "Tuxtla Gutierrez", "C", "23445322");
		Client clientTwo = new Client("Fernanda", "Samuel", "López", "Gómez", "9612368450", "7ma norte poniente #1850, Av. San Francisco", "Tuxtla Gutierrez", "P", "23445323");
		Client clientThree = new Client("Pedro", "", "Hernández", "Amber", "9612958469", "7ma norte poniente #1850, Av. San Francisco", "Tuxtla Gutierrez", "P", "23445324");
		Client clientFour = new Client("Roberto", "Samuel", "Camil", "Robles", "9852368469", "7ma norte poniente #1850, Av. San Francisco", "Tuxtla Gutierrez", "P", "23445325");
		Client clientFive = new Client("Francisco", "", "Oaxaca", "Camacho", "9619568469", "7ma norte poniente #1850, Av. San Francisco", "Tuxtla Gutierrez", "C", "23445326");
		Client clientSix = new Client("Manuela", "", "Méndez", "Torres", "9612368169", "7ma norte poniente #1850, Av. San Francisco", "Tuxtla Gutierrez", "C", "23445327");
	
		this.groupClients.put(Long.valueOf(0), clientOne);
		this.groupClients.put(Long.valueOf(1), clientTwo);
		this.groupClients.put(Long.valueOf(2), clientThree);
		this.groupClients.put(Long.valueOf(3), clientFour);
		this.groupClients.put(Long.valueOf(4), clientFive);
		this.groupClients.put(Long.valueOf(5), clientSix);
	}

	public Map<Long, Client> getGroupClients() {
		return groupClients;
	}

	public void setGroupClients(Map<Long, Client> groupClients) {
		this.groupClients = groupClients;
	}
	
}
