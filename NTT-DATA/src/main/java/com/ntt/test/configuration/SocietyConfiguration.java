package com.ntt.test.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ntt.test.model.Society;

@Configuration
public class SocietyConfiguration {
	
	@Bean
	public Society society() {
		return new Society();
	}

}
