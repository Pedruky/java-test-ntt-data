package com.ntt.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.ntt.test.model.Client;
import com.ntt.test.model.Society;

@RestController
@RequestMapping("/test")
@ResponseBody
@CrossOrigin(origins= "*")
public class RestSocietyController {
	
	@Autowired
	private Society society;
	
	@PostMapping()
	public ResponseEntity<Client> findClientByDocTypeAndNum(@RequestBody ObjectNode json){
		String documentType = "";
		String documentNumber = "";
		try {
			documentType = json.get("documentType").toString().replaceAll("\"", "");
			documentNumber = json.get("documentNumber").toString().replaceAll("\"", "");
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The Document Type and/or Document Number is wrong (CODE 400)\n");
        }
		if(documentNumber.compareTo("") == 0 || documentType.compareTo("") == 0) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The Document Type and/or Document Number is wrong (CODE 400)\n");
		}
		if(documentType.compareTo("C") == 0 && documentType.compareTo("P") == 0) {
			throw new ResponseStatusException(HttpStatus.OK, "Document type client is not found(CODE 200)\n");
		}
		Client clientFinded = null;
		for(Long index = Long.parseLong("0"); index < society.getGroupClients().size(); index++) {
			System.out.println(society.getGroupClients().get(index).getDocumentType() + " = " + documentType);
			System.out.println(society.getGroupClients().get(index).getDocumentNumber() + " = " + documentNumber);
			if(society.getGroupClients().get(index).getDocumentType().equals(documentType) && society.getGroupClients().get(index).getDocumentNumber().equals(documentNumber)) {
				clientFinded = society.getGroupClients().get(index);
				System.out.println(clientFinded);
			}
		}
		if(clientFinded == null) {
			throw new ResponseStatusException(HttpStatus.OK, "Client not found(CODE 200)\n");
		}else {
			return new ResponseEntity<Client>(clientFinded, HttpStatus.OK);
		}
	}
	
	

}
